# Cosmos Odyssey
> Web App called “Cosmos Odyssey” that shows the best deals for our demanding customers in our solar system.
> Live demo [_here_](https://cosmos.agencypin.com).

## Table of Contents
* [General Info](#general-information)
* [Requirements](#requirements)
* [Setup](#setup)

## General Information
Customers must be able to select travel between the different planets and the system should show possible routes-based prices. After careful consideration customer can choose to make a reservation to their name on a specific route.

## Requirements
- PHP >= 7.2
- Apache Web Server with mod_rewrite enabled
- Latest stable v4 Phalcon Framework extension enabled (4.1.2)
- MySQL >= 5.5 or MariaDB >= 10.6

This project have dependencies that require Node.js together with npm & Composer

## Setup
Install Phalcon PHP extension to your apache server. Visit
https://docs.phalcon.io/4.0/en/installation for further information.

Install Composer locally or globally by following the instructions listed [_here_](https://getcomposer.org/doc/00-intro.md).

Install MariaDB to your server. Follow instructions here: https://www.mariadbtutorial.com/getting-started/install-mariadb/

Create database (navigate to cosmosodyssey/db and find install_db.sql). Run SQL queries.

Make sure you have Node version >= 8.0 and npm >= 5 or Yarn.
```
# clone repository
git clone https://dronyxxx@bitbucket.org/dronyxxx/cosmosodyssey.git

# change the directory
cd cosmosodyssey

# install the npm dependencies listed on package.json
npm install

# install the composer dependencies listed on composer.json
composer install

# change config file
cd app/config
cp config_example.php config.php

# edit file
vi config.php
```

Change the following properties only

"host"        => "",
"username"    => "",
"password"    => "",
"dbname"      => ""

Change app/cache directory permissions
```
chmod -R 777 cache/
```

Run application in your favourite browser