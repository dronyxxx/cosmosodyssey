const webpack = require('webpack');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const fs = require('fs-extra');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
    mode: 'production',
    entry: {
        "public": "./public.js",
        "system": "./vendor.js"
    },
    output: {
        path: path.resolve(__dirname, 'public/assets/vendor'),
        publicPath: '/public/assets/vendor/',
        filename: "[name].[contenthash:8].js",
        chunkFilename: '[name].[contenthash:8].js',
    },
    plugins: [
        new CleanWebpackPlugin(),
        new webpack.ProvidePlugin({
            $: 'jquery',
            jquery: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash:8].css'
        }),
        new webpack.IgnorePlugin(/^codemirror$/),
        new HtmlWebpackPlugin({
            filename: 'main.volt',
            template: 'app/backend/views/layouts/main_webpack_template.volt',
            chunks: ['system'],
            inject: false,
            minify: false,
            cache: false
        }),
        {
            apply: (compiler) => {
                compiler.hooks.afterEmit.tap('AfterEmitPlugin', (compilation) => {

                    let options = {
                        overwrite: true
                    };

                    fs.copy(__dirname + '/public/assets/vendor/main.volt', __dirname + '/app/backend/views/layouts/main.volt', options, err => {
                        if (err) return console.error(err);
                        console.log('Copy build success!');
                    });
                });
            }
        }
    ],
    optimization: {
        minimize: true,
        minimizer: [
            new TerserJSPlugin({
                terserOptions: {
                    output: {
                        comments: false,
                    },
                },
                extractComments: false,
            }),
            new OptimizeCSSAssetsPlugin({
                cssProcessorPluginOptions: {
                    preset: ['default', {discardComments: {removeAll: true}}],
                }
            })
        ]
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    MiniCssExtractPlugin.loader, 'css-loader'
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'
                ]
            },
            {
                test: /\.ts$/,
                use: [
                    'ts-loader'
                ]
            },
            {
                test: /\.(gif|png|woff|woff2|eot|ttf|svg)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: '[name].[hash:8].[ext]',
                            outputPath: "img"
                        }
                    }
                ]
            },
            {
                test: require.resolve('jquery'),
                use: [
                    {
                        loader: 'expose-loader',
                        options: 'jQuery'
                    },
                    {
                        loader: 'expose-loader',
                        options: '$'
                    }
                ]
            }
        ]
    }
};