/**
 * @constructor
 * @property {string}  VERSION      - Build Version.
 * @property {string}  AUTHOR       - Author.
 * @property {string}  SUPPORT      - Support Email.
 * @property {string}  pageScrollElement  - Scroll Element in Page.
 * @property {object}  $body - Cache Body.
 */

var Cosmos = function () {
    this.VERSION = "1.0.0";
    this.AUTHOR = "Cosmos";
    this.SUPPORT = "andrei@agencypin.com";

    this.pageScrollElement = 'html, body';
    this.$body = $('body');
};

Cosmos.prototype = {
    /** @function bindGlobal
     * @description Global bindings for events that often occur
     */
    bindGlobal: function (_this) {

        $(document).on('click', '.reserve_button', function (e) {
            e.preventDefault();
            $(document).find(".card").each(function(){
                $(this).removeClass('bg-info');
                $(this).find('.reserve_button').text('').text('reserve');
                $(this).find('.reserve_button_selected').text('').text('reserve');
            });
            $(document).find('#show-submit').closest('div').addClass('d-none');
            $(document).find('.show-after-selection').addClass('d-none');

            $(this).removeClass('reserve_button');
            $(this).addClass('reserve_button_selected');
            $(this).text('').text('cancel reservation');
            $(this).closest('.card').addClass('bg-info');
            $(document).find('#show-submit').closest('div').removeClass('d-none');
            $(document).find('.show-after-selection').removeClass('d-none');
        });

        $(document).on('click', '.reserve_button_selected', function (e) {
            e.preventDefault();
            $(document).find(".card").each(function(){
                $(this).removeClass('bg-info');
                $(this).find('.reserve_button').text('').text('reserve');
                $(this).find('.reserve_button_selected').text('').text('reserve');
            });
            $(document).find('#show-submit').closest('div').addClass('d-none');
            $(this).removeClass('reserve_button_selected');
            $(this).addClass('reserve_button');
            $(document).find('.show-after-selection').addClass('d-none');
        });

        $(document).on('click', '#show-routes', function (e) {
            e.preventDefault();
            var form = $(document).find("#customer-form");
            $(document).find("#error-messages").html('').hide();
            $(document).find("#available-routes").html('').html('...calculating best routes');
            $.ajax({
                data: {
                    ajax_action: 'get_available_routes',
                    form_data: form.serialize()
                },
                method: "POST",
                success: function (response) {
                    var status_code = response.status.code;
                    if (status_code == 'success') {
                        $(document).find("#available-routes").html('');
                        if (response.data.route_html.length > 0) {
                            $.each(response.data.route_html, function (route_index, route_data) {
                                $(document).find("#available-routes").append(route_data);
                            });
                            $(document).find("#available-routes").show();
                            $(document).find(".show-after-results").removeClass('d-none');
                        }
                        clearInterval(_this.countdownTimer);
                        _this.initCountdown(response.data.pricelist_valid_until);
                    }else if (status_code == 'warning') {
                        if (response.data.error_messages.length > 0) {
                            $.each(response.data.error_messages, function (index, error_message) {
                                var error_message_div = '<div class="text-danger">' + error_message + '</div>';
                                $(document).find("#error-messages").append(error_message_div);
                                $(document).find("#error-messages").show();
                            });
                        }
                        $(document).find("#available-routes").html('');
                    }else if (status_code == 'db_error'){
                        alert(response.status.message);
                    }
                },
                complete: function () {
                },
                error: function(jqXHR, exception){
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    alert(msg);
                }
            });
        });

        $(document).on('click', '#show-submit', function (e) {
            e.preventDefault();
            var form = $(document).find("#customer-form");
            var route_ids = [];
            $(document).find(".bg-info").find('.route_id').each(function(){
                route_ids.push($(this).data('route-id'));
            });
            var firstname = $(document).find("#firstname").val();
            var lastname = $(document).find("#lastname").val();
            var error = false;
            if (!firstname){
                alert('Firstname is required');
                error = true;
            }
            if (!lastname){
                alert('Lastname is required');
                error = true;
            }
            if (error){
                return false;
            }
            $.ajax({
                data: {
                    ajax_action: 'add_reservation',
                    form_data: form.serialize(),
                    routes: route_ids
                },
                method: "POST",
                success: function (response) {
                    var status_code = response.status.code;
                    if (status_code == 'success') {
                        $(document).find("#available-routes").html('');
                        if (response.data.route_html.length > 0) {
                            $.each(response.data.route_html, function (route_index, route_data) {
                                $(document).find("#available-routes").append(route_data);
                            });
                            $(document).find("#available-routes").show();
                            $(document).find(".show-after-results").removeClass('d-none');
                        }
                        $(document).find('#show-submit').closest('div').addClass('d-none');
                        $(document).find('.show-after-selection').addClass('d-none');
                    }else if (status_code == 'warning') {
                        if (response.data.error_messages.length > 0) {
                            $.each(response.data.error_messages, function (index, error_message) {
                                var error_message_div = '<div class="text-danger">' + error_message + '</div>';
                                $(document).find("#error-messages").append(error_message_div);
                                $(document).find("#error-messages").show();
                            });
                        }
                        $(document).find("#available-routes").html('');
                    }else if (status_code == 'db_error'){
                        alert(response.status.message);
                    }
                },
                complete: function () {
                },
                error: function(jqXHR, exception){
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    alert(msg);
                }
            });
        });
    },

    /** @function init
     * @description Initialize all core components.
     */
    init: function () {
        this.bindGlobal(this);
        this.initCountdown(parseInt($(document).find('#countdown').data('valid-until')));
    },

    initCountdown: function(validUntil){
        var _this = this;
        var d = new Date();
        var timeleft = parseInt(validUntil - (d.getTime() / 1000));
        _this.countdownTimer = setInterval(function(){
            if(timeleft <= 0){
                clearInterval(_this.countdownTimer);
                $(document).find('#countdown').html('').html('PRICELIST EXPIRED!');
                $(document).find('#show-submit').closest('div').addClass('d-none');
                $(document).find('#available-routes').html('');
            } else {
                $(document).find('#countdown').html('').html('Current pricelist '+timeleft + " seconds remaining");
            }
            timeleft -= 1;
        }, 1000);
    },
    countdownTimer: false,
};
var Core = $.Cosmos = new Cosmos();
$.Cosmos.Constructor = Cosmos;