'use strict';

module.exports = function (grunt) {
    var config = {
        version: '1.0.0',
        core: 'public/assets',
        src: 'src',
        dist: 'dist'
    };
    grunt.initConfig({
        config: config,
        pkg: grunt.file.readJSON('package.json'),

        // Watcher for CSS and JS changes
        watch: {
            css: {
                files: ['<%= config.src %>/scss/**/*.scss', '<%= config.src %>/scss/*.scss'],
                tasks: ['dist-css', 'copy:css', 'clean:dist'],
                options: {
                    spawn: false
                }
            },
            js: {
                files: ['<%= config.src %>/js/*.js'],
                tasks: ['dist-js', 'copy:js', 'clean:dist'],
                options: {
                    spawn: false
                }
            }
        },

        // Sync JS and CSS in browser
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        '<%= config.core %>/css/*.css',
                        '<%= config.core %>/js/*.js'
                    ]
                }
            }
        },

        clean: {
            options: {force: true},
            dist: '<%= config.dist %>'
        },

        // Copy compiled CSS and JS into the project
        copy: {
            css: {
                files: [{
                    src: ['**/*.min.*'],
                    expand: true,
                    flatten: true,
                    filter: 'isFile',
                    cwd: '<%= config.dist %>/css',
                    dest: '<%= config.core %>/css'
                }]
            },
            js: {
                files: [{
                    src: ['**/*.min.*'],
                    expand: true,
                    flatten: true,
                    filter: 'isFile',
                    cwd: '<%= config.dist %>/js',
                    dest: '<%= config.core %>/js'
                }]
            }
        },

        // Compile TypeScript
        ts: {
            options: {
                rootDir: "<%= config.src %>/ts",
                sourceMap: false
            },
            default: {
                src: ["<%= config.src %>/ts/*.ts"],
                outDir: "<%= config.dist %>/js"
            }
        },

        // Compile SCSS files
        sass: {
            dist: {
                options: {
                    style: 'expanded',
                    cacheLocation: '<%= config.src %>/cache/scss'
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.src %>/scss/build',
                    src: ['*.scss'],
                    dest: '<%= config.dist %>/css',
                    ext: '.css'
                }]
            }
        },

        // Minify dist CSS
        cssmin: {
            core: {
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>/css',
                    src: ['**/*.css', '!**/*.min.css'],
                    dest: '<%= config.dist %>/css',
                    ext: '.min.css'
                }]
            }
        },

        // Create core.js combining components
        concat: {
            core_main: {
                files: {
                    '<%= config.dist %>/js/cosmos.js': [
                        '<%= config.src %>/js/core.js',
                        '<%= config.src %>/js/ui/*.js',
                        '<%= config.src %>/js/core.init.js'
                    ]
                }
            }
        },

        // Uglify JS to create *.min versions
        uglify: {
            options: {
                mangle: false,
                sourceMap: true
            },
            core: {
                files: [{
                    expand: true,
                    cwd: '<%= config.dist %>/js',
                    src: ['**/*.js', '!**/*.min.js'],
                    dest: '<%= config.dist %>/js',
                    ext: '.min.js'
                }]
            }
        }
    });

    // These plugins loads necessary tasks.
    require('load-grunt-tasks')(grunt, {
        scope: 'devDependencies'
    });
    require('time-grunt')(grunt);

    grunt.registerTask("prepareModules", "Finds and prepares modules for concatenation.", function () {
        var concat = grunt.config.get('concat') || {};
        grunt.config.set('concat', concat);
    });

    // Dist JS.
    grunt.registerTask('dist-js', ['prepareModules', 'concat', 'uglify']);

    // Dist TS.
    grunt.registerTask('dist-ts', ['ts', 'uglify']);

    // Dist CSS
    grunt.registerTask('dist-css', ['sass', 'cssmin']);

    // Copy files from dist to core
    grunt.registerTask('dist-copy-js', ['copy:js']);

    // Core build task (Creates dist folder)
    grunt.registerTask('dist', ['clean:dist', 'dist-css', 'dist-js']);

    // Erase dist folder
    grunt.registerTask('dist-clean', ['clean:dist']);

    grunt.registerTask('test-watch', ['browserSync', 'watch']);

    //One time compile JS
    grunt.registerTask('compile-js', ['dist-js', 'copy:js', 'clean:dist']);

    //One time compile TS
    grunt.registerTask('compile-ts', ['dist-ts', 'copy:js', 'clean:dist']);

    //One time compile CSS
    grunt.registerTask('compile-css', ['dist-css', 'copy:css', 'clean:dist']);

    //One time compile ALL
    grunt.registerTask('compile-all', ['dist-js', 'dist-css', 'copy:js', 'copy:css', 'clean:dist']);

    // Default task.
    grunt.registerTask('default', ['watch']);
};