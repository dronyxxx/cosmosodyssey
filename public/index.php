<?php

/**
 * NB! Before installing this project you need to install additional libraries via composer. Check for composer.json
 */

include __DIR__ . "/../app/LoadSystem.php";
$system = new LoadSystem();
$system->boot();
$system->setPublicSystem();
