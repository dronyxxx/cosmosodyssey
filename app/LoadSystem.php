<?php

/**
 * Main class to set global variables, routes and load all necessary modules
 */

include __DIR__ . '/../vendor/autoload.php';

class LoadSystem
{
    protected \Phalcon\Config $config;
    protected \Phalcon\DI\FactoryDefault $di;
    protected Phalcon\Loader $loader;

    public function __construct()
    {
        $this->setConfig(unserialize($this->setDefaultConfig()));
        $this->setDi(new \Phalcon\DI\FactoryDefault());
        $this->setLoader(new \Phalcon\Loader());
    }

    public function setDefaultConfig() : string
    {
        include_once __DIR__ . '/config/config.php';
        return CONFIG;
    }

    public function boot()
    {
        $_this = $this;

        foreach ($_this->getConfig()->application as $name => $path) {
            $this->getLoader()->registerDirs([$name => $path], true)->register();
        }

        $this->getLoader()->registerNamespaces(
            [
                'Backend' => '../app/backend/',
                'Library' => '../app/library/',
                'Library\Helpers' => '../app/library/helpers/',
                'Library\Plugins' => '../app/library/plugins/',
                'Library\Validators' => '../app/library/validators/',
            ],
            true
        )->register();

        $this->di->setShared('loader', function () use ($_this) {
            return $_this->getLoader();
        });

        $this->di->set('profiler', function () {
            return new \Phalcon\Db\Profiler();
        });

        $this->di->setShared('annotations', function () {
            return new \Phalcon\Annotations\Adapter\Memory();
        });

        $this->di->setShared('modelsMetadata', function () {
            return new \Phalcon\Mvc\Model\Metadata\Memory();
        });

        $this->di->setShared('transactions', function () {
            return new \Phalcon\Mvc\Model\Transaction\Manager();
        });

        $this->di->setShared('url', function () use ($_this) {
            $url = new \Phalcon\Url();
            $url->setBaseUri($_this->getConfig()->settings->baseUri);
            return $url;
        });

        $this->di->set('forms', function () {
            return new \Phalcon\Forms\Manager();
        });

        $this->di->setShared('application', function () use ($_this) {
            return new \Phalcon\Mvc\Application($_this->getDi());
        });

        $this->di->setShared("cookies", function () {
            $cookies = new \Phalcon\Http\Response\Cookies();
            $cookies->useEncryption(false);
            return $cookies;
        });

        $this->di->setShared('session', function () {
            $session = new \Phalcon\Session\Manager();
            $files = new \Phalcon\Session\Adapter\Stream(
                [
                    'savePath' => '/tmp',
                ]
            );
            $session->setAdapter($files);
            return $session;
        });

        $this->di->setShared('sessionBag', function () {
            return new \Phalcon\Session\Bag("sessionBag");
        });

        /**
         * Custom variables
         */
        $this->di->setShared('config', function () use ($_this) {
            return $_this->getConfig();
        });

        $this->di->setShared('modelsManager', function () {
            return new \Phalcon\Mvc\Model\Manager();
        });

        $this->di->setShared('assets', function () {
            return new \Phalcon\Assets\Manager();
        });

        $this->di->setShared('cosmos_api', function () {
            return new \Library\Plugins\CosmosApi();
        });

        $this->di->set('themeUri', function () use ($_this) {
            return $_this->getConfig()->settings->themeUri;
        });

        $this->di->setShared('message', function () {
            return new AjaxMessage();
        });

        $this->di->setShared('ajax_response', function () {
            return new ResponseObject();
        });

        $this->di->setShared('helper', function () {
            return new Library\Helpers\ApplicationHelper();
        });

        $this->di->setShared('scss', function () {
            return new ScssPhp\ScssPhp\Compiler();
        });

    }

    private function setRouter()
    {
        $_this = $this;
        $this->di->set('router', function () use ($_this) {

            $router = new \Phalcon\Mvc\Router();
            $router->removeExtraSlashes(true);

            $router->add('/', array(
                'module' => 'Application',
                'controller' => 'index',
                'action' => 'index',
                'namespace' => 'Backend\Controllers\Application'
            ));

            $router->add('/:controller', array(
                'module' => 'Application',
                'controller' => 1,
                'action' => 'index',
                'namespace' => 'Backend\Controllers\Application'
            ));

            $router->add('/:controller/:action', array(
                'module' => 'Application',
                'controller' => 1,
                'action' => 2,
                'namespace' => 'Backend\Controllers\Application'
            ));

            $router->add('/:controller/:action/:params', array(
                'module' => 'Application',
                'controller' => 1,
                'action' => 2,
                'params' => 3,
                'namespace' => 'Backend\Controllers\Application'
            ));

            $uri = $_this->getDi()->get('request')->getURI();
            $router->handle($uri);
            return $router;
        });
    }

    public function setPublicSystem()
    {
        $_this = $this;
        $this->setRouter();
        $router = $this->getDi()->get('router');

        switch ($router->getModuleName()) {
            case 'Application':

                $this->connectToDatabase();

                $this->di->set('dispatcher', function () use ($_this) {

                    $eventsManager = new \Phalcon\Events\Manager();
                    $dispatcher = new \Phalcon\Mvc\Dispatcher();

                    $eventsManager->attach('dispatch', new \Library\Plugins\Security());

                    $eventsManager->attach('dispatch:beforeDispatch', function ($event, $dispatcher) use ($_this) {
                        if ($_this->getDi()->get('request')->isAjax() && $dispatcher->wasForwarded()) {
                            $_this->loadModule($dispatcher->getModuleName());
                        }
                    });

                    $dispatcher->setEventsManager($eventsManager);
                    return $dispatcher;
                });

                $this->handleBackendApplication();
                break;
        }
    }

    private function handleBackendApplication()
    {
        $modules_to_register['Application'] = array(
            'className' => 'Backend\\Application',
            'path' => '../app/backend/Application.php',
            'controllersNamespace' => 'Backend\Controllers\\Application'
        );
        $this->getDi()->getShared('application')->registerModules($modules_to_register);

        $router = $this->getDi()->get('router');

        $dispatcher = $this->getDi()->get('dispatcher');
        $dispatcher->setDefaultNamespace($router->getNamespaceName());
        $dispatcher->setNamespaceName($router->getNamespaceName());
        $dispatcher->setModuleName($router->getModuleName());
        if ($router->getControllerName()) $dispatcher->setControllerName($router->getControllerName());
        if ($router->getActionName()) $dispatcher->setActionName($router->getActionName());
        if ($router->getParams()) $dispatcher->setParams($router->getParams());
        $this->getDi()->set('dispatcher', $dispatcher);

        try {
            $this->loadModule($router->getModuleName());
            $this->getDi()->get('dispatcher')->dispatch();
        } catch (\Phalcon\Exception $exception) {
            if ($this->getDi()->get('request')->isAjax()) {
                $ajax_response = $this->getDi()->getShared('ajax_response');
                $errors = $ajax_response->getErrors();
                if (!$errors) {
                    $ajax_response->setStatus($this->getDi()->get('message')->getDbErrorMessage($exception->getMessage()));
                }
                $ajax_response->getJSONResponse()->send();
            }
        }

        $response = $this->getDi()->get('response');

        if (!$response->isSent()) {
            if ($this->getDi()->get('request')->isAjax()) {
                $view = $this->getDi()->getShared('view');
                if (!$response->getContent() && !$view->isDisabled()) {
                    $view->start();
                    $view->render(
                        $dispatcher->getControllerName(),
                        $dispatcher->getActionName(),
                        $dispatcher->getParams()
                    );
                    $view->finish();
                    $response->setContent($view->getContent());
                }
                $response->send();
            } else {
                $view = $this->getDi()->getShared('view');
                $disabled = $view->isDisabled();
                if (!$disabled) {
                    $view->start();
                    $view->render(
                        $dispatcher->getControllerName(),
                        $dispatcher->getActionName(),
                        $dispatcher->getParams()
                    );
                    $view->finish();
                    if ($response instanceof Phalcon\Http\ResponseInterface) {
                        $response->setContent($view->getContent());
                        $response->send();
                    } else {
                        echo 'Application response is not valid';
                        die();
                    }
                } else {
                    $response->send();
                }
            }
        }
    }

    private function loadModule($module_name)
    {
        if ($module_name != 'Application'){
            $module_class_name = 'Backend\\Application';
            $module = new $module_class_name;
            $module->registerAutoloaders($this->getDi());
        }
        $module_class_name = 'Backend\\' . $module_name;
        $module = new $module_class_name;
        $module->registerAutoloaders($this->getDi());
        $module->registerServices($this->getDi());
    }

    private function connectToDatabase()
    {
        $_this = $this;
        $this->getDi()->setShared('db', function () use ($_this) {
            $eventsManager = new \Phalcon\Events\Manager();
            $dialect = new MysqlExtended();
            $dialect->registerCustomFunction(
                'REGEXP',
                function ($dialect, $expression) {
                    $arguments = $expression['arguments'];
                    return sprintf(
                        "%s REGEXP(%s)",
                        $dialect->getSqlExpression($arguments[0]),
                        $dialect->getSqlExpression($arguments[1])
                    );
                }
            );

            try {
                $connection = new Phalcon\Db\Adapter\Pdo\Mysql([
                    'host' => $_this->getConfig()->database->host,
                    'username' => $_this->getConfig()->database->username,
                    'password' => $_this->getConfig()->database->password,
                    'dbname' => $_this->getConfig()->database->dbname,
                    'charset' => $_this->getConfig()->database->charset,
                    'dialectClass' => $dialect,
                    'persistent' => true
                ]);
                $connection->setEventsManager($eventsManager);
                return $connection;
            } catch (Exception $e) {
                die("<b>Error when initializing database connection:</b> " . $e->getMessage());
            }
        });
    }

    /**
     * @return \Phalcon\Config
     */
    public function getConfig(): \Phalcon\Config
    {
        return $this->config;
    }

    /**
     * @param \Phalcon\Config $config
     */
    public function setConfig(\Phalcon\Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return \Phalcon\DI\FactoryDefault
     */
    public function getDi(): \Phalcon\DI\FactoryDefault
    {
        return $this->di;
    }

    /**
     * @param \Phalcon\DI\FactoryDefault $di
     */
    public function setDi(\Phalcon\DI\FactoryDefault $di)
    {
        $this->di = $di;
    }

    /**
     * @return \Phalcon\Loader
     */
    public function getLoader(): \Phalcon\Loader
    {
        return $this->loader;
    }

    /**
     * @param \Phalcon\Loader $loader
     */
    public function setLoader(\Phalcon\Loader $loader)
    {
        $this->loader = $loader;
    }

}