<?php

use Phalcon\Di\Injectable;

class ResponseObject extends Injectable
{
    protected $errors;
    protected $html;
    protected $response;
    protected $customfields = [];
    protected $ajax_message;
    protected $status;
    protected $set_response_header = false;
    protected $status_code = 422;
    protected $data;

    public function __construct($response = null)
    {
        if ($response){
            $this->response = $response;
        }else{
            $this->response = \Phalcon\DI::getDefault()->get('response');
        }
        $this->ajax_message = new AjaxMessage();
    }

    public function getHtml()
    {
        return $this->html;
    }

    public function setHtml($html)
    {
        $this->html = $html;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function setErrors(array $errors)
    {
        $this->errors = $errors;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->status_code;
    }

    /**
     * @param int $status_code
     */
    public function setStatusCode(int $status_code)
    {
        $this->status_code = $status_code;
    }


    public function addError($error, $key = null) {
        if ($key){
            $this->errors[$key] = $error;
        }else{
            $this->errors[] = $error;
        }
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function setSetResponseHeader($set_response_header)
    {
        $this->set_response_header = $set_response_header;
        if ($set_response_header){
            $this->response->setStatusCode($this->status_code);
        }
    }

    public function clear()
    {
        $this->errors = [];
        $this->status = null;
    }

    public function setResponse($status, $errors) {
        $this->errors = $errors;
        $this->status = $status;
    }

    public function addResponseField($field, $value)
    {
        $this->customfields[$field] = $value;
    }

    public function setCustomFieldsArray($array)
    {
        $this->customfields = $array;
    }

    public function getResponse()
    {
        $response = array();

        if(!empty($this->errors)){
            $response['errors'] = $this->errors;
        }

        if($this->html !== null ){
            $response['html'] = $this->html;
        }

        if($this->data !== null) {
            $response['data'] = $this->data;
        }

        if($this->status !== null) {
            $response['status'] = $this->status;
        }

        foreach($this->customfields as $key =>$field){
            $response[$key] = $field;
        }
        return $response;
    }

    public function send()
    {
        $this->response->send();
    }

    public function getJSONResponse()
    {
        $response = $this->getResponse();
        $http_Response = $this->response;
        $http_Response->setJsonContent($response);
        return $http_Response;
    }

    /**
     * @throws \Phalcon\Mvc\Dispatcher\Exception
     */
    public function throwAjaxException($message, $element = null)
    {
        if (is_array($element) && !empty($element)){
            foreach ($element as $key => $error_message) {
                $this->addError($error_message, $key);
            }
        }elseif (is_object($element)){
            foreach ($element->getMessages() as $error_message) {
                $this->addError($error_message->getMessage(), $error_message->getField());
            }
        }elseif ($element != ''){
            $this->addError($element);
        }

        $this->setStatus($this->ajax_message->getValidationErrorMessage($message));
        throw new \Phalcon\Mvc\Dispatcher\Exception($message, $this->getStatusCode());
    }
}