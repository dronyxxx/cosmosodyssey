<?php

use Phalcon\Di\Injectable;

class AjaxMessage extends Injectable
{
    const SUCCESS = 'success';
    const WARNING = 'warning';
    const DANGER = 'danger';
    const VALIDATION_ERROR = 'error';
    const DB_ERROR = 'db_error';
    const COMPLETE = 'complete';
    const INFO = 'info';

    public function getAjaxMessage($message, $code): array
    {
        return array('message' => $message, 'code' => $code);
    }

    public function getSuccessMessage($message): array
    {
        return $this->getAjaxMessage($message, self::SUCCESS);
    }

    public function getWarningMessage($message): array
    {
        return $this->getAjaxMessage($message, self::WARNING);
    }

    public function getDangerMessage($message): array
    {
        return $this->getAjaxMessage($message, self::DANGER);
    }

    public function getValidationErrorMessage($message): array
    {
        return $this->getAjaxMessage($message, self::VALIDATION_ERROR);
    }

    public function getDbErrorMessage($message): array
    {
        return $this->getAjaxMessage($message, self::DB_ERROR);
    }

    public function getCompleteMessage($message): array
    {
        return $this->getAjaxMessage($message, self::COMPLETE);
    }

    public function getInfoMessage($message): array
    {
        return $this->getAjaxMessage($message, self::INFO);
    }
}