<?php

namespace Library\Plugins;

use Phalcon\Di\Injectable;

class AjaxUrl extends Injectable
{
    /**
     * Forward ajax request
     */
    public function forwardAjaxUrl($dispatcher): bool
    {
        if ($this->request->isAjax() && $this->request->getPost('ajax_action')) {
            switch ($this->request->getPost('ajax_action')) {
                case 'get_available_routes':
                    $module = 'Application';
                    $dispatcher->setModuleName($module);
                    $dispatcher->setNamespaceName('Backend\Controllers\Application');
                    $dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'getroutes'
                    ));
                    break;

                case 'add_reservation':
                    $module = 'Application';
                    $dispatcher->setModuleName($module);
                    $dispatcher->setNamespaceName('Backend\Controllers\Application');
                    $dispatcher->forward(array(
                        'controller' => 'index',
                        'action' => 'addreservation'
                    ));
                    break;
            }
            unset($_POST['ajax_action']);
        }
        return false;
    }
}