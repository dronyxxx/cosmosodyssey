<?php

namespace Library\Plugins;

use Phalcon\Dispatcher\Exception as DispatcherException;

use Phalcon\Di\Injectable,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Acl\Adapter\Memory as AclList,
    Phalcon\Acl\Enum as PhalconAcl,
    Phalcon\Acl\Role;

class Security extends Injectable
{
    public $role;

    public function getAcl()
    {
        if (!isset($this->persistent->backend_acl)){

            $acl = new AclList();

            $acl->setDefaultAction(PhalconAcl::DENY);

            $role = new Role($this->role);
            $acl->addRole($role);

            $acl->allow($this->role, '*', '*');

            $this->persistent->backend_acl = $acl;
            $this->persistent->role = $this->role;
        }
        return $this->persistent->backend_acl;
    }

    /**
     * @throws DispatcherException
     */
    public function beforeExecuteRoute(\Phalcon\Events\Event $event, Dispatcher $dispatcher): bool
    {
        $this->role = 'BackendGuest';

        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        if (isset($this->persistent->role) && $this->persistent->role != $this->role){
            $this->persistent->clear();
        }

        $backend_acl = $this->getAcl();
        $allowed = $backend_acl->isAllowed($this->role, $controller, $action);

        if ($this->request->isAjax() && !$dispatcher->wasForwarded()){
            $allowed = true;
        }

        if ($allowed != PhalconAcl::ALLOW && $this->role != 'BackendGuest') {
            throw new DispatcherException("showPrivate", 400);
        }

        if ($this->request->isAjax() && isset($_POST['ajax_action']) && !$dispatcher->wasForwarded()) {
            $ajax_url = new AjaxUrl();
            $ajax_url->forwardAjaxUrl($dispatcher);
            return false;
        }
        return true;
    }

    /**
     * @throws \Exception
     */
    public function beforeException(\Phalcon\Events\Event $event, Dispatcher $dispatcher, $exception)
    {
        $application_module = new \Backend\Application();
        $application_module->registerAutoloaders($this->getDi());
        $application_module->registerServices($this->getDi());

        $dispatcher->setModuleName("Application");
        $dispatcher->setNamespaceName($dispatcher->getDefaultNamespace());
        $dispatcher->setControllerName("errors");
        $dispatcher->setActionName('show404');

        if ($exception instanceof DispatcherException) {
            switch ($exception->getCode()) {
                case 400:
                    $dispatcher->setActionName('showPrivate');
                    break;
                case 401:
                    $dispatcher->setActionName('showDisabled');
                    break;
                case 402:
                    $dispatcher->setActionName('showMaintenanceMode');
            }
            $dispatcher->dispatch();
        } else {
            echo $exception->getMessage();
            die();
        }
    }
}