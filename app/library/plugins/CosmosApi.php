<?php

namespace Library\Plugins;

use Cosmos\SolarSystem;
use Library\Helpers\ApplicationHelper;
use Phalcon\Di\Injectable;
use Phalcon\Exception;

/**
 * @property ApplicationHelper helper
 */
class CosmosApi extends Injectable
{
    /**
     * Get latest valid price list from DB. If list does not exist call API and store the result to Db
     * @throws \Phalcon\Exception
     */
    public function getLatestValidPricelist(): \Phalcon\Mvc\ModelInterface
    {
        $pricelist = \Pricelist::findFirst([
            'order' => 'valid_until DESC'
        ]);

        if (!$pricelist || $pricelist->getValidUntil() < time()){
            $cosmos_data_from_api = $this->sendApiRequest();
            if (!isset($cosmos_data_from_api)){
                throw new Exception("Can not get Cosmos data from API");
            }
            // inset new data if latest pricelist became invalid
            $pricelist = $this->insertCosmosData($cosmos_data_from_api);
        }
        // remove old price lists
        $this->deleteCosmosPricelist();
        return $pricelist;
    }

    /**
     * @return array
     * Get latest price list from API. Return json decoded array
     */
    private static function sendApiRequest(): array
    {
        $ch = curl_init();
        try {
            curl_setopt($ch, CURLOPT_URL, 'https://cosmos-odyssey.azurewebsites.net/api/v1.0/TravelPrices');
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60);
            curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            $body = curl_exec($ch);
        } finally {
            if (!is_null($ch)) {
                curl_close($ch);
            }
        }
        return json_decode($body, true);
    }

    /**
     * Insert API object data to database
     * @throws Exception
     */
    private function insertCosmosData($cosmos_data_from_api): \Phalcon\Mvc\ModelInterface
    {
        $planet_system = new SolarSystem();
        $planet_system->setPlanets();

        // create providers list first
        foreach ($cosmos_data_from_api['legs'] as $leg){
            foreach ($leg['providers'] as $provider){
                $provider_entity = \Provider::findFirst([
                    'conditions' => 'name = :name:',
                    'bind' => [
                        'name' => trim($provider['company']['name'])
                    ]
                ]);
                if (!$provider_entity){
                    $provider_entity = new \Provider();
                    $provider_entity->setApiId($provider['company']['id']);
                    $provider_entity->setName(trim($provider['company']['name']));
                    if (!$provider_entity->save()){
                        foreach ($provider_entity->getMessages() as $message) {
                            throw new Exception($message->getMessage());
                        }
                    }
                }
            }
        }

        // prepare new price list for DB
        $pricelist = new \Pricelist();
        $pricelist->setApiId($cosmos_data_from_api['id']);
        $pricelist->setValidUntil(strtotime($cosmos_data_from_api['validUntil']));
        $pricelist->setCreatedAt(time());

        $routes = [];
        foreach ($cosmos_data_from_api['legs'] as $leg){
            $route = new \Route();

            $route_info = $leg['routeInfo'];
            // set current route info and add to database
            $route->setApiId($route_info['id']);
            $route->setDistance($route_info['distance']);
            $route->setFromPlanetId($planet_system->getPlanetIdByName($route_info['from']['name']));
            $route->setToPlanetId($planet_system->getPlanetIdByName($route_info['to']['name']));
            $route->setPlanetSystemId($planet_system->getId());

            $route_providers_entities = [];
            foreach ($leg['providers'] as $provider){

                // create new provider if not created previously
                $provider_entity = \Provider::findFirst([
                   'conditions' => 'name = :name:',
                   'bind' => [
                       'name' => trim($provider['company']['name'])
                   ]
                ]);
                if (!$provider_entity){
                    $provider_entity = new \Provider();
                    $provider_entity->setApiId($provider['company']['id']);
                    $provider_entity->setName(trim($provider['company']['name']));
                }

                // create new root provider and assign provider
                $route_provider_entity = new \RouteProvider();
                $route_provider_entity->Provider = $provider_entity;
                $route_provider_entity->setApiId($provider['id']);
                $route_provider_entity->setPrice($provider['price']);
                $route_provider_entity->setFlightStart(strtotime($provider['flightStart']));
                $route_provider_entity->setFlightEnd(strtotime($provider['flightEnd']));

                $route_providers_entities[] = $route_provider_entity;
            }
            $route->RouteProvider = $route_providers_entities;
            $routes[] = $route;
        }
        $pricelist->Route = $routes;

        // save price list
        if (!$pricelist->save()){
            foreach ($pricelist->getMessages() as $message) {
                throw new Exception($message->getMessage());
            }
        }
        return $pricelist;
    }

    /**
     * This method is set to delete old pricelists from the database.
     * Will skip to delete last 15 actual pricelists.
     * NB! All associated reservations will be deleted from DB
     * @throws Exception
     */
    private static function deleteCosmosPricelist()
    {
        $pricelists_to_delete = \Pricelist::find([
            'order' => 'valid_until DESC',
            'limit' => [
                'number' => 10000,
                'offset' => 5
            ]
        ]);
        foreach ($pricelists_to_delete as $pricelist_to_delete){
            if (!$pricelist_to_delete->delete()){
                foreach ($pricelist_to_delete->getMessages() as $message) {
                    throw new Exception($message->getMessage());
                }
            }
        }
    }

}