<?php
namespace Library\Helpers;

use Cosmos\SolarSystem;
use Phalcon\Di\Injectable;

class ApplicationHelper extends Injectable
{
    /**
     * @param $pricelist
     * @return array
     * Return all available planets in latest pricelist for Solar System
     */
    public function getAllPlanetsFromPricelist($pricelist): array
    {
        $planet_system = new SolarSystem();
        $planet_system->setPlanets();

        $pricelist_routes = $pricelist->getRelated('Route', [
            'planet_system_id = :planet_system_id:',
            'bind' => [
                'planet_system_id' => $planet_system->getId()
            ],
        ]);
        $all_planets = [];
        foreach ($pricelist_routes as $route_entity){
            if ($planet_system->getPlanetById($route_entity->getFromPlanetId()) && !isset($all_planets[$route_entity->getFromPlanetId()])) {
                $all_planets[$route_entity->getFromPlanetId()] = $planet_system->getPlanetNameById($route_entity->getFromPlanetId());
            }
            if ($planet_system->getPlanetById($route_entity->getToPlanetId()) && !isset($all_planets[$route_entity->getToPlanetId()])) {
                $all_planets[$route_entity->getToPlanetId()] = $planet_system->getPlanetNameById($route_entity->getToPlanetId());
            }
        }
        return $all_planets;
    }

    /**
     * @param $element
     * @return string
     * Render form elements. Overwrite default behaviour
     */
    function getCustomRenderedElement($element): string
    {
        if ($element->getLabel()){
            return $element->label().' '.$element;
        }
        return $element;
    }

    /**
     * @param $input
     * @return array|array[]
     * Get all possible variants for selected route
     */
    function getCartesianArray($input): array
    {
        $result = array(array());
        foreach ($input as $key => $values) {
            $append = array();
            foreach($result as $product) {
                foreach($values as $item) {
                    $product[$key] = $item;
                    $append[] = $product;
                }
            }
            $result = $append;
        }
        return $result;
    }

    /**
     * @param $ss
     * @return string
     * Convert timestamp to human readable values
     */
    function seconds2human($ss): string
    {
        $s = $ss%60;
        $m = floor(($ss%3600)/60);
        $h = floor(($ss%86400)/3600);
        $d = floor(($ss%2592000)/86400);
        $M = floor($ss/2592000);
        return "$M months, $d days, $h hours, $m minutes, $s seconds";
    }
}