<?php

namespace Cosmos;

use Library\Helpers\ApplicationHelper;

/**
 * @property ApplicationHelper helper
 */
class Planet extends SolarSystem
{
    public int $id;
    public string $name;
    public array $destinations = [];
    public array $planet_path = [];
    public bool $destination_planet = false;
    public bool $checked_in_route = false;
    public bool $valid_planet_in_route = true;
    public bool $added_to_root = true;
    public int $valid_path_number = 0;

    /**
     * Recursive method to get current planet possible routes until destination planet
     */
    public function getPlanetRoutes()
    {
        foreach ($this->getDestinations() as $destination_planet){
            if ($destination_planet->isCheckedInRoute()){
                $this->setCheckedInRoute(true);
            }elseif ($destination_planet->isDestinationPlanet()) {
                $this->planet_path[$destination_planet->getValidPathNumber()][] = $destination_planet->getId();
                $this->planet_path[$destination_planet->getValidPathNumber()][] = $this->getId();
                $destination_planet->setValidPathNumber($destination_planet->getValidPathNumber()+1);
            }else{
                $this->setCheckedInRoute(true);
                $destination_planet->getPlanetRoutes();
                if (!empty($destination_planet->getPlanetPath())){
                    $this->setPlanetPath($destination_planet->getPlanetPath());
                    foreach ($this->getPlanetPath() as $key => $path){
                        $this->planet_path[$key][] = $this->getId();
                    }
                }
            }
        }
    }

    /**
     * reset planet variables for next possible routes to check. Called from SolarSystem class
     */
    public function clearPlanet()
    {
        $this->setCheckedInRoute(false);
        $this->setPlanetPath([]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getDestinations(): array
    {
        return $this->destinations;
    }

    /**
     * @param array $destinations
     */
    public function setDestinations(array $destinations): void
    {
        $this->destinations = $destinations;
    }

    /**
     * @return array
     */
    public function getPlanetPath(): array
    {
        return $this->planet_path;
    }

    /**
     * @param array $planet_path
     */
    public function setPlanetPath(array $planet_path): void
    {
        $this->planet_path = $planet_path;
    }

    /**
     * @return bool
     */
    public function isDestinationPlanet(): bool
    {
        return $this->destination_planet;
    }

    /**
     * @param bool $destination_planet
     */
    public function setDestinationPlanet(bool $destination_planet): void
    {
        $this->destination_planet = $destination_planet;
    }

    /**
     * @return bool
     */
    public function isCheckedInRoute(): bool
    {
        return $this->checked_in_route;
    }

    /**
     * @param bool $checked_in_route
     */
    public function setCheckedInRoute(bool $checked_in_route): void
    {
        $this->checked_in_route = $checked_in_route;
    }

    /**
     * @return bool
     */
    public function isValidPlanetInRoute(): bool
    {
        return $this->valid_planet_in_route;
    }

    /**
     * @param bool $valid_planet_in_route
     */
    public function setValidPlanetInRoute(bool $valid_planet_in_route): void
    {
        $this->valid_planet_in_route = $valid_planet_in_route;
    }

    /**
     * @return bool
     */
    public function isAddedToRoot(): bool
    {
        return $this->added_to_root;
    }

    /**
     * @param bool $added_to_root
     */
    public function setAddedToRoot(bool $added_to_root): void
    {
        $this->added_to_root = $added_to_root;
    }

    /**
     * @return int
     */
    public function getValidPathNumber(): int
    {
        return $this->valid_path_number;
    }

    /**
     * @param int $valid_path_number
     */
    public function setValidPathNumber(int $valid_path_number): void
    {
        $this->valid_path_number = $valid_path_number;
    }

}