<?php

namespace Cosmos;

use Library\Helpers\ApplicationHelper;
use Phalcon\Di\Injectable;

/**
 * @property ApplicationHelper helper
 */
class SolarSystem extends Injectable
{
    public int $id;
    public string $name;
    public array $planets;
    protected int $customer_origin_planet_id;
    protected int $customer_destination_planet_id;
    public array $path = [];

    public function __construct()
    {
        $this->id = 1;
        $this->name = 'Solar system';
    }

    public function setPlanets(): void
    {
        $mercury = new Planet();
        $mercury->setId(1);
        $mercury->setName('Mercury');
        $this->planets[] = $mercury;

        $venus = new Planet();
        $venus->setId(2);
        $venus->setName('Venus');
        $this->planets[] = $venus;

        $earth = new Planet();
        $earth->setId(3);
        $earth->setName('Earth');
        $this->planets[] = $earth;

        $mars = new Planet();
        $mars->setId(4);
        $mars->setName('Mars');
        $this->planets[] = $mars;

        $jupiter = new Planet();
        $jupiter->setId(5);
        $jupiter->setName('Jupiter');
        $this->planets[] = $jupiter;

        $saturn = new Planet();
        $saturn->setId(6);
        $saturn->setName('Saturn');
        $this->planets[] = $saturn;

        $uranus = new Planet();
        $uranus->setId(7);
        $uranus->setName('Uranus');
        $this->planets[] = $uranus;

        $neptune = new Planet();
        $neptune->setId(8);
        $neptune->setName('Neptune');
        $this->planets[] = $neptune;

        // set possible destinations
        $mercury->setDestinations([$venus]);
        $venus->setDestinations([$mercury, $earth]);
        $earth->setDestinations([$uranus, $jupiter]);
        $mars->setDestinations([$venus]);
        $jupiter->setDestinations([$venus, $mars]);
        $saturn->setDestinations([$earth, $neptune]);
        $uranus->setDestinations([$saturn, $neptune]);
        $neptune->setDestinations([$mercury, $uranus]);
    }

    public function getRoutes(): array
    {
        $this->setPlanets();

        $origin_planet = $this->getPlanetById($this->customer_origin_planet_id);
        $origin_planet->setCheckedInRoute(true);

        $destination_planet = $this->getPlanetById($this->customer_destination_planet_id);
        $destination_planet->setDestinationPlanet(true);

        $this->getAllRoutes($origin_planet);
        return $this->path;
    }

    /**
     * @param $origin_planet
     * Collect all possible routes for selected path
     */
    public function getAllRoutes($origin_planet)
    {
        $routes = [];
        foreach ($origin_planet->getDestinations() as $destination_planet){
            if (!$destination_planet->isDestinationPlanet()){
                $destination_planet->getPlanetRoutes();
                if (!empty($destination_planet->getPlanetPath())){
                    foreach ($destination_planet->getPlanetPath() as $key => $path){
                        $destination_planet->planet_path[$key][] = $origin_planet->getId();
                    }
                    $routes[] = $destination_planet->planet_path;
                }
            }else{
                $destination_planet->planet_path[0][0] = $destination_planet->getId();
                $destination_planet->planet_path[0][1] = $origin_planet->getId();
                $routes[] = $destination_planet->planet_path;
            }
            $this->clearPlanetsRoutes();
            $origin_planet->setCheckedInRoute(true);
        }

        foreach($routes as $planet_routes){
            if (!empty($planet_routes)){
                foreach ($planet_routes as $planet_route){
                    $this->path[] = $planet_route;
                }
            }
        }
    }

    /**
     * Clear Planet
     */
    public function clearPlanetsRoutes()
    {
        foreach ($this->planets as $planet){
            $planet->clearPlanet();
        }
    }

    /**
     * @param $name
     * @return int
     */
    public function getPlanetIdByName($name): int
    {
        $id = 0;
        foreach ($this->planets as $planet){
            if ($planet->getName() == $name) return $planet->getId();
        }
        return $id;
    }

    /**
     * @param $id
     * @return string
     */
    public function getPlanetNameById($id): string
    {
        $name = '';
        foreach ($this->planets as $planet){
            if ($planet->getId() == $id) return $planet->getName();
        }
        return $name;
    }

    /**
     * @param $id
     * @return Planet
     */
    public function getPlanetById($id): Planet
    {
        $planet = null;
        foreach ($this->planets as $planet){
            if ($planet->getId() == $id) return $planet;
        }
        return $planet;
    }

    /**
     * @return array
     */
    public function getPlanets(): array
    {
        return $this->planets;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getCustomerOriginPlanetId(): int
    {
        return $this->customer_origin_planet_id;
    }

    /**
     * @param int $customer_origin_planet_id
     */
    public function setCustomerOriginPlanetId(int $customer_origin_planet_id): void
    {
        $this->customer_origin_planet_id = $customer_origin_planet_id;
    }

    /**
     * @return int
     */
    public function getCustomerDestinationPlanetId(): int
    {
        return $this->customer_destination_planet_id;
    }

    /**
     * @param int $customer_destination_planet_id
     */
    public function setCustomerDestinationPlanetId(int $customer_destination_planet_id): void
    {
        $this->customer_destination_planet_id = $customer_destination_planet_id;
    }


}