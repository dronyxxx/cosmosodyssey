<div class="container-xs-height full-height">
    <div class="row-xs-height">
        <div class="col-xs-height col-middle">
            <div class="error-container text-center">
                <h1 class="error-number">404</h1>
                <h2 class="semi-bold">{{ translate._("Sorry but we couldnt find this page") }}</h2>
                <p>{{ translate._("The page you are looking for does not exsist.") }}
                </p>
            </div>
        </div>
    </div>
</div>