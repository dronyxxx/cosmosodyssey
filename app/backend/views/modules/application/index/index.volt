<div class="text-center" style="padding-top: 40px; padding-bottom: 20px">
    <h1>Cosmos Odyssey</h1>
</div>
<div style="padding-top: 20px">
    <form id="customer-form">
        <div class="row no-gutters justify-content-center">
            <div class="col-md-4">
                <div id="countdown" class="text-center text-danger" data-valid-until="{{ validUntil }}" style="padding-bottom: 20px;"></div>
                <div id="error-messages" class="text-center" style="padding-bottom: 20px;"></div>
                {% for element in customer_form %}

                    {% if element.getAttribute('pass') %}
                        {% continue %}
                    {% endif %}

                    {% if is_a(element, 'Phalcon\Forms\Element\Hidden') %}
                        {{ customer_form.render(element.getName()) }}
                    {% else %}
                        {% if element.getAttribute('stick_field') %}
                            {% if element.getAttribute('stick_field') != 'skip' %}
                                {% do customer_form.get(element.getAttribute('stick_field')).setAttribute('pass', true) %}
                            {% endif %}
                            <div class="row {% if element.getAttribute('row_class') %}{{ element.getAttribute('row_class') }}{% endif %}">
                                <div class="form-group col-md-6">
                                    {{ customer_form.render(element.getName()) }}
                                </div>
                                <div class="form-group col-md-6">
                                    {{ customer_form.render(element.getAttribute('stick_field')) }}
                                </div>
                            </div>
                        {% else %}
                            <div class="form-group">
                                {{ customer_form.render(element.getName()) }}
                            </div>
                        {% endif %}
                    {% endif %}
                {% endfor %}
                <div>
                    <button type="button" class="btn btn--button btn-primary btn-block" id="show-routes">
                        Show available routes
                    </button>
                </div>
                <div class="d-none" style="padding-top: 10px;">
                    <button type="button" class="btn btn--button btn-warning btn-block" id="show-submit">
                        Submit reservation
                    </button>
                </div>
                <div class="text-center" style="padding-top: 20px;">
                    <div class="accordion" id="available-routes"></div>
                </div>
            </div>
        </div>
    </form>
</div>

