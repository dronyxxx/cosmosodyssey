<div class="card">
    <div class="card-header" id="heading{{ route_nr }}">
        Planets to fly through:<br>
        {% for route_planet in route_planets %}
            {{ route_planet }}&nbsp;
        {% endfor %}
        <button type="button" class="btn btn--button btn-primary btn-block reserve_button">
            reserve
        </button>
    </div>
    <div>
        <div class="card-body">
            <div>Total distance: {{ total_distance }}</div>
            <div>Travel time: {{ helper.seconds2human(travel_time) }}</div>
            <div>Total price: {{ total_price }}</div>
            <div style="padding-top: 10px;">
                <b>Fight data:</b>
                {% for route_array_data in route_array  %}
                <div style="padding-bottom: 10px;" class="route_id" data-route-id="{{ route_array_data.getId() }}">
                    <div>
                        From planet: {{ planet_system.getPlanetNameById(route_array_data.getRelated('Route').getFromPlanetId()) }}
                    </div>
                    <div>
                        To planet: {{ planet_system.getPlanetNameById(route_array_data.getRelated('Route').getToPlanetId()) }}
                    </div>
                    <div>
                        Flight start time: {{ date('d.m.Y H:i:s', route_array_data.getFlightStart()) }}
                    </div>
                    <div>
                        Flight end time: {{ date('d.m.Y H:i:s', route_array_data.getFlightEnd()) }}
                    </div>
                    <div>
                        Flight price: {{ route_array_data.getPrice() }}
                    </div>
                    <div>
                       Provider: {{ route_array_data.getRelated('Provider').getName() }}
                    </div>
                    <div>
                        Distance: {{ route_array_data.getRelated('Route').getDistance() }}
                    </div>
                </div>

                {% endfor %}
            </div>
        </div>
    </div>
</div>