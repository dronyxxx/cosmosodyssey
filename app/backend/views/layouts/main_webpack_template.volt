<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    {{ get_title() }}
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-title" content="Cosmos" />
    <meta name="application-name" content="Cosmos" />
    <meta name="theme-color" content="#ffffff" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="default" />

    <% for (var css in htmlWebpackPlugin.files.css) { %>
        <link href="<%= htmlWebpackPlugin.files.css[css] %>" rel="stylesheet">
    <% } %>
    <% for (var js in htmlWebpackPlugin.files.js) { %>
        <script src="<%= htmlWebpackPlugin.files.js[js] %>"></script>
    <% } %>

    {{ stylesheet_link('public/assets/css/cosmos.min.css') }}
</head>
<body>
    {{ content() }}
    {{ javascript_include('public/assets/js/cosmos.min.js') }}
</body>
</html>