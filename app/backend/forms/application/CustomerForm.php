<?php

namespace Backend\Forms\Application;

use Library\Helpers\ApplicationHelper;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\PresenceOf;

/**
 * @property ApplicationHelper helper
 */
class CustomerForm extends Form {

    public $entity;

    public function __construct($entity = null, $pricelist_entity = null, $validUntil = 0)
    {
        if (!$entity){
            $entity = new \CustomerReservation();
        }
        parent::__construct($entity);

        $this->entity = $entity;

        $this->add(new Hidden('id'));
        $this->add(new Hidden('pricelist_id', [
            'value' => $pricelist_entity->getId()
        ]));
        if ($validUntil){
            $this->add(new Hidden('valid_until', [
                'value' => $validUntil
            ]));
        }

        $field = new Select('travel_origin', $this->helper->getAllPlanetsFromPricelist($pricelist_entity), [
            'stick_field' => 'travel_destination'
        ]);
        $field->setLabel('Travel origin');
        $this->add($field);

        $field = new Select('travel_destination', $this->helper->getAllPlanetsFromPricelist($pricelist_entity));
        $field->setLabel('Travel destination');
        $this->add($field);

        $field = new Select('filter_by', [1 => 'Distance (lower first)', 2 => 'Distance (higher first)', 3 => 'Price (lower first)', 4 => 'Price (higher first)', 5 => 'Travel time (lower first)', '6' => 'Travel time (higher first)'], [
            'row_class' => 'show-after-results d-none',
            'stick_field' => 'route_provider'
        ]);
        $field->setAttribute('value',1);
        $field->setLabel('Filter by:');
        $this->add($field);

        $field = new Select('route_provider', \Provider::find(), [
            'using' => [
                'id', 'name'
            ],
            'row_class' => 'show-after-results d-none',
            'useEmpty' => true,
            'emptyText' => 'all providers',
            'emptyValue' => null
        ]);
        $field->setLabel('Route provider:');
        $this->add($field);

        $field = new Text('firstname', [
            'row_class' => 'show-after-selection d-none',
            'stick_field' => 'lastname'
        ]);
        $field->setAttribute('required', true);
        $field->addValidator(new PresenceOf([
            'message' => 'Firstame is required'
        ]));
        $field->setLabel('Firstname');
        $this->add($field);

        $field = new Text('lastname', [
            'row_class' => 'show-after-selection d-none',
        ]);
        $field->setAttribute('required', true);
        $field->addValidator(new PresenceOf([
            'message' => 'Lastname is required'
        ]));
        $field->setLabel('Lastname');
        $this->add($field);
    }

    public function render($name, $attributes = []): string
    {
        $element = $this->get($name);
        return $this->helper->getCustomRenderedElement($element);
    }
}