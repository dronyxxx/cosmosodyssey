<?php

/**
 * Main module class to load paths and register necessary services
 */

namespace Backend;

use Phalcon\Mvc\ModuleDefinitionInterface,
    Phalcon\Di\DiInterface,
    Phalcon\Exception,
    Phalcon\Mvc\View,
    Phalcon\Mvc\View\Engine\Volt as VoltEngine,
    Phalcon\Events\Manager as EventsManager;
use Phalcon\Events\Event;

class Application implements ModuleDefinitionInterface
{
    /**
     * @param DiInterface|null $dependencyInjector
     */
    public function registerAutoloaders(DiInterface $dependencyInjector = null)
    {
        $loader = $dependencyInjector->getShared('loader');
        $loader->registerNamespaces(
            array(
                'Backend\Controllers\Application' => __DIR__.'/controllers/application/',
                'Backend\Forms\Application' => __DIR__.'/forms/application/',
                'Cosmos' => __DIR__.'/cosmos/'
            ),
            true
        );
        $loader->register();
    }

    /**
     * @param DiInterface|null $dependencyInjector
     */
    public function registerServices (DiInterface $dependencyInjector = null)
    {
        $dependencyInjector->setShared('view', function() use ($dependencyInjector) {
            $eventsManager = new EventsManager();

            $eventsManager->attach("view", function(Event $event, $view) {
                if ($event->getType() == 'notFoundView') {
                    throw new Exception('View not found in' . $view->getActiveRenderPath());
                }
            });

            $config = $dependencyInjector->getShared('config');

            $view = new View();
            $view->registerEngines(array(
                '.volt' => function ($view) use ($config) {
                    $volt = new VoltEngine($view, $this);
                    $volt->setOptions(array(
                        'path' => $config->settings->cacheDir,
                        'separator' => '_'
                    ));
                    $compiler = $volt->getCompiler();
                    $compiler->addFunction('is_a', 'is_a');
                    $compiler->addFunction('substr', 'substr');
                    $compiler->addFunction('count', 'count');
                    $compiler->addFunction('isset', 'isset');
                    $compiler->addFunction('in_array', 'in_array');
                    $compiler->addFunction('is_array', 'is_array');
                    $compiler->addFunction('array_diff', 'array_diff');
                    $compiler->addFunction('array_push', 'array_push');
                    $compiler->addFunction('checkempty', 'empty');
                    $compiler->addFunction('date', 'date');
                    $compiler->addFunction('date_format', 'date_format');
                    $compiler->addFunction('date_create', 'date_create');
                    $compiler->addFunction('strtotime', 'strtotime');
                    $compiler->addFunction('time', 'time');
                    $compiler->addFunction('array_key_exists', 'array_key_exists');
                    $compiler->addFunction('trim', 'trim');
                    $compiler->addFunction('floatval', 'floatval');
                    $compiler->addFunction('nl2br', 'nl2br');
                    $compiler->addFunction('utf8_decode', 'utf8_decode');
                    return $volt;
                }
            ));

            $view->disableLevel([
                View::LEVEL_LAYOUT => true,
                View::LEVEL_MAIN_LAYOUT => true
            ]);

            $view->setBasePath(__DIR__ . '/views');
            $view->setViewsDir('/modules/application/');
            $view->setLayoutsDir('../../layouts/');
            $view->setTemplateAfter('main');

            $view->setVar("layout_parials", '../../layouts/partials');

            $view->setEventsManager($eventsManager);

            return $view;
        });
    }
}