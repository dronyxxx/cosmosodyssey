<?php
declare(strict_types=1);
namespace Backend\Controllers\Application;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    public function initialize()
    {
        $this->view->setVar("controller", $this->dispatcher->getControllerName());
    }
}
