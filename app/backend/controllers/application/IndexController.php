<?php
declare(strict_types=1);
namespace Backend\Controllers\Application;

use Backend\Forms\Application\CustomerForm;
use Cosmos\SolarSystem;
use Library\Helpers\ApplicationHelper;
use Library\Plugins\CosmosApi;
use Phalcon\Exception;

/**
 * @property ApplicationHelper helper
 * @property CosmosApi cosmos_api
 * @property \ResponseObject ajax_response
 * @property \AjaxMessage message
 */
class IndexController extends ControllerBase
{
    public function indexAction()
    {
        try {
            $pricelist = $this->cosmos_api->getLatestValidPricelist();
            $validUntil = $pricelist->getValidUntil();
            $this->view->setVar('validUntil', $validUntil);
            $this->view->setVar('customer_form', new CustomerForm(new \CustomerReservation(), $pricelist, $validUntil));
        } catch (\Phalcon\Exception $e) {
            print_r($e->getMessage());
            die();
        }
    }

    /**
     * getRoutesAction method is called via AJAX request
     * to return all possible routes for customer selection
     */
    public function getRoutesAction()
    {
        $this->view->disable();

        if (!$this->request->isAjax()) {
            exit('Application response is not valid.');
        }

        $form_data = [];
        if (!empty($this->request->getPost('form_data'))) {
            parse_str($this->request->getPost('form_data'), $form_data);
        }

        $error_messages = [];

        if (!isset($form_data['travel_origin']) || !$form_data['travel_origin']) {
            $error_messages[] = 'Travel origin is not set';
        }

        if (!isset($form_data['travel_destination']) || !$form_data['travel_destination']) {
            $error_messages[] = 'Travel destination is not set';
        }

        if (isset($form_data['travel_origin']) && isset($form_data['travel_destination']) && $form_data['travel_destination'] == $form_data['travel_origin']) {
            $error_messages[] = 'There are no routes available for selection';
        }

        if (!empty($error_messages)){
            $this->ajax_response->setData(['error_messages' => $error_messages]);
            $this->ajax_response->setStatus($this->message->getWarningMessage("Error!"));
            return $this->ajax_response->getJSONResponse()->send();
        }

        try {
            $planet_system = new SolarSystem();
            $planet_system->setCustomerOriginPlanetId(intval($form_data['travel_origin']));
            $planet_system->setCustomerDestinationPlanetId(intval($form_data['travel_destination']));
            $possible_routes = $planet_system->getRoutes();

            if (empty($possible_routes)){
                throw new Exception('There are no routes available for selection');
            }

            $pricelist = $this->cosmos_api->getLatestValidPricelist();

            $path_routes = [];
            foreach ($possible_routes as $key => $possible_route){
                // loop through possible routes between planets
                $possible_route = array_reverse($possible_route);
                $from_planet_id = $possible_route[0]; // set first planet as flight origin
                unset($possible_route[0]);

                $available_route_variants = [];
                foreach ($possible_route as $route_key => $to_planet_id){
                    // get available route from DB
                    $pricelist_route_entity = $pricelist->getRelated('Route', [
                        'planet_system_id = :system_id: AND from_planet_id = :from: AND to_planet_id = :to:',
                        'bind' => [
                            'system_id' => $planet_system->getId(),
                            'from' => $from_planet_id,
                            'to' => $to_planet_id
                        ]
                    ])->getFirst();
                    // get available providers for route from DB
                    $route_providers = $pricelist_route_entity->getRelated('RouteProvider');
                    foreach ($route_providers as $route_provider){
                        // filter out route providers if set in search form
                        if (isset($form_data['route_provider']) && $form_data['route_provider'] && $form_data['route_provider'] != $route_provider->getProviderId()){
                            continue;
                        }
                        $available_route_variants[$route_key][] = $route_provider;
                    }
                    // reset origin planet
                    $from_planet_id = $to_planet_id;
                }
                // get all route variants as cartesian array
                $path_routes[] = $this->helper->getCartesianArray($available_route_variants);
            }

            $all_routes_variants = [];
            // combine possible routes to one single array and get data for each route variant
            foreach ($path_routes as $path_route){
                foreach ($path_route as $route_variant){
                    $route_data = [];

                    $total_distance = $total_price = $travel_time = 0;
                    $planets = [];
                    $is_available_route = true;
                    $last_flight_end = 0;
                    foreach ($route_variant as $route_provider){
                        // filter out all flights where start time is lesser then previous flight end time
                        if ($last_flight_end > 0 && $route_provider->getFlightStart() < $last_flight_end){
                            $is_available_route = false;
                            break;
                        }
                        // get all data for route
                        $total_distance += intval($route_provider->getrelated('Route')->getDistance());
                        $total_price += floatval($route_provider->getPrice());
                        $flight_duration = (intval($route_provider->getFlightEnd()) - intval($route_provider->getFlightStart()));
                        $travel_time += $flight_duration;
                        $planets[$route_provider->getRelated('Route')->getFromPlanetId()] = $planet_system->getPlanetNameById($route_provider->getRelated('Route')->getFromPlanetId());
                        $planets[$route_provider->getRelated('Route')->getToPlanetId()] = $planet_system->getPlanetNameById($route_provider->getRelated('Route')->getToPlanetId());
                        $last_flight_end = $route_provider->getFlightEnd();
                    }

                    if (!$is_available_route){
                        continue; // skip unavailable routes
                    }

                    $route_data['route_array'] = $route_variant;
                    $route_data['route_planets'] = $planets;
                    $route_data['total_distance'] = $total_distance;
                    $route_data['total_price'] = $total_price;
                    $route_data['travel_time'] = $travel_time;

                    $all_routes_variants[] = $route_data;
                }
            }

            // sort routes by selected search values
            $filter = intval($form_data['filter_by']);
            if ($filter == 1 || !$filter){
                // default sorting
                usort($all_routes_variants, function($a, $b) {
                    return $a['total_distance'] <=> $b['total_distance'];
                });
            }elseif ($filter == 2){
                usort($all_routes_variants, function($a, $b) {
                    return $b['total_distance'] <=> $a['total_distance'];
                });
            }elseif ($filter == 3){
                usort($all_routes_variants, function($a, $b) {
                    return $a['total_price'] <=> $b['total_price'];
                });
            }elseif ($filter == 4){
                usort($all_routes_variants, function($a, $b) {
                    return $b['total_price'] <=> $a['total_price'];
                });
            }elseif ($filter == 5){
                usort($all_routes_variants, function($a, $b) {
                    return $a['travel_time'] <=> $b['travel_time'];
                });
            }elseif ($filter == 6){
                usort($all_routes_variants, function($a, $b) {
                    return $b['travel_time'] <=> $a['travel_time'];
                });
            }

            foreach ($all_routes_variants as $route_nr => $route_variant){
                // get route html and parse into return array
                $data_to_return['route_html'][] = $this->view->getRender('index/partials', 'route', [
                    'route_nr' => $route_nr,
                    'total_distance' => $route_variant['total_distance'],
                    'total_price' => $route_variant['total_price'],
                    'travel_time' => $route_variant['travel_time'],
                    'route_planets' => $route_variant['route_planets'],
                    'route_array' => $route_variant['route_array'],
                    'planet_system' => $planet_system
                ]);
            }

            // if no routes available
            if (empty($data_to_return['route_html'])){
                throw new Exception('There are no routes available for selection');
            }

            // send value double check price list validity
            $data_to_return['pricelist_valid_until'] = $pricelist->getValidUntil();

            $this->ajax_response->setStatus($this->message->getSuccessMessage("Success"));
            $this->ajax_response->setData($data_to_return);
        } catch (\Phalcon\Exception $e) {
            $this->ajax_response->setData(['error_messages' => [$e->getMessage()]]);
            $this->ajax_response->setStatus($this->message->getWarningMessage("Error!"));
            return $this->ajax_response->getJSONResponse()->send();
        }
        return $this->ajax_response->getJSONResponse()->send();
    }

    /**
     * Add reservation data for customer via AJAX request
     */
    public function addReservationAction()
    {
        $this->view->disable();

        if (!$this->request->isAjax()) {
            exit('Application response is not valid.');
        }

        $form_data = [];
        if (!empty($this->request->getPost('form_data'))) {
            parse_str($this->request->getPost('form_data'), $form_data);
        }

        $error_messages = [];

        if (!$this->request->getPost('routes') || empty($this->request->getPost('routes'))) {
            $error_messages[] = 'Reservation route is not set';
        }

        if (!empty($error_messages)){
            $this->ajax_response->setData(['error_messages' => $error_messages]);
            $this->ajax_response->setStatus($this->message->getWarningMessage("Error!"));
            return $this->ajax_response->getJSONResponse()->send();
        }

        try {
            $pricelist = \Pricelist::findFirst($form_data['pricelist_id']);
            $customer_reservation_entity = new \CustomerReservation();
            $reservation_form = new CustomerForm($customer_reservation_entity, $pricelist);
            //pass form data to form validation. Entity values are set automatically after check
            if ($reservation_form->isValid($form_data)) {
                $customer_reservation_entity->setId(null);
                $customer_reservation_entity->setCreatedAt(time());
                $route_entities = [];
                // loop through selected route and store to DB
                foreach ($this->request->getPost('routes') as $route_provider_id){
                    $route_provider = \RouteProvider::findFirst($route_provider_id);
                    $route_entity = new \ReservationRoute();
                    $route_entity->setRouteProviderId(intval($route_provider->getId()));
                    $route_entities[] = $route_entity;
                }
                $customer_reservation_entity->ReservationRoute = $route_entities;
                if (!$customer_reservation_entity->save()){
                    foreach ($customer_reservation_entity->getMessages() as $message) {
                        throw new Exception($message->getMessage());
                    }
                }
            }else{
                throw new Exception('Form is not valid!');
            }
        } catch (\Phalcon\Exception $e) {
            $this->ajax_response->setData(['error_messages' => [$e->getMessage()]]);
            $this->ajax_response->setStatus($this->message->getWarningMessage("Error!"));
            return $this->ajax_response->getJSONResponse()->send();
        }

        $this->ajax_response->setStatus($this->message->getSuccessMessage("Success"));
        $this->ajax_response->setData([]);
        return $this->ajax_response->getJSONResponse()->send();
    }
}

