<?php

ini_set("display_errors", "On");
error_reporting(E_ALL);

$mainConfig = new \Phalcon\Config([
    "database" => [
        "adapter"     => "Mysql",
        "host"        => "",
        "username"    => "",
        "password"    => "",
        "dbname"      => "",
        "charset"     => "utf8"
    ],
    "application" => [
        "libraryPath"       => "../app/library/",
        "modelsDir"     => __DIR__ ."/../models/",
        "extensionsDir"       => "../app/library/extensions/",
        "componentsDir"     => "../app/library/components/"
    ],
    "settings" => [
        "baseUri"        => "/",
        "themeUri"        => "public/",
        "cacheDir"       => __DIR__ . "/../cache/",
    ]
]);
define("CONFIG", serialize($mainConfig));
return $mainConfig;