<?php

class RouteProvider extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $api_id;

    /**
     *
     * @var integer
     */
    protected $provider_id;

    /**
     *
     * @var integer
     */
    protected $route_id;

    /**
     *
     * @var double
     */
    protected $price;

    /**
     *
     * @var integer
     */
    protected $flight_start;

    /**
     *
     * @var integer
     */
    protected $flight_end;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field api_id
     *
     * @param string $api_id
     * @return $this
     */
    public function setApiId($api_id)
    {
        $this->api_id = $api_id;

        return $this;
    }

    /**
     * Method to set the value of field provider_id
     *
     * @param integer $provider_id
     * @return $this
     */
    public function setProviderId($provider_id)
    {
        $this->provider_id = $provider_id;

        return $this;
    }

    /**
     * Method to set the value of field route_id
     *
     * @param integer $route_id
     * @return $this
     */
    public function setRouteId($route_id)
    {
        $this->route_id = $route_id;

        return $this;
    }

    /**
     * Method to set the value of field price
     *
     * @param double $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Method to set the value of field flight_start
     *
     * @param integer $flight_start
     * @return $this
     */
    public function setFlightStart($flight_start)
    {
        $this->flight_start = $flight_start;

        return $this;
    }

    /**
     * Method to set the value of field flight_end
     *
     * @param integer $flight_end
     * @return $this
     */
    public function setFlightEnd($flight_end)
    {
        $this->flight_end = $flight_end;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field api_id
     *
     * @return string
     */
    public function getApiId()
    {
        return $this->api_id;
    }

    /**
     * Returns the value of field provider_id
     *
     * @return integer
     */
    public function getProviderId()
    {
        return $this->provider_id;
    }

    /**
     * Returns the value of field route_id
     *
     * @return integer
     */
    public function getRouteId()
    {
        return $this->route_id;
    }

    /**
     * Returns the value of field price
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Returns the value of field flight_start
     *
     * @return integer
     */
    public function getFlightStart()
    {
        return $this->flight_start;
    }

    /**
     * Returns the value of field flight_end
     *
     * @return integer
     */
    public function getFlightEnd()
    {
        return $this->flight_end;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("route_provider");
        $this->belongsTo('provider_id', 'Provider', 'id', ['alias' => 'Provider']);
        $this->belongsTo('route_id', 'Route', 'id', ['alias' => 'Route']);
        $this->hasMany('id', 'ReservationRoute', 'route_provider_id', ['alias' => 'ReservationRoute']);

    }
}
