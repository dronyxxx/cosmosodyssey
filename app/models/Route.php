<?php

class Route extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var string
     */
    protected $api_id;

    /**
     *
     * @var integer
     */
    protected $distance;

    /**
     *
     * @var integer
     */
    protected $from_planet_id;

    /**
     *
     * @var integer
     */
    protected $pricelist_id;

    /**
     *
     * @var integer
     */
    protected $to_planet_id;

    /**
     *
     * @var integer
     */
    protected $planet_system_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field api_id
     *
     * @param string $api_id
     * @return $this
     */
    public function setApiId($api_id)
    {
        $this->api_id = $api_id;

        return $this;
    }

    /**
     * Method to set the value of field distance
     *
     * @param integer $distance
     * @return $this
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field api_id
     *
     * @return string
     */
    public function getApiId()
    {
        return $this->api_id;
    }

    /**
     * Returns the value of field distance
     *
     * @return integer
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @return mixed
     */
    public function getFromPlanetId()
    {
        return $this->from_planet_id;
    }

    /**
     * @param mixed $from_planet_id
     */
    public function setFromPlanetId($from_planet_id): void
    {
        $this->from_planet_id = $from_planet_id;
    }

    /**
     * @return mixed
     */
    public function getPricelistId()
    {
        return $this->pricelist_id;
    }

    /**
     * @param mixed $pricelist_id
     */
    public function setPricelistId($pricelist_id): void
    {
        $this->pricelist_id = $pricelist_id;
    }

    /**
     * @return mixed
     */
    public function getToPlanetId()
    {
        return $this->to_planet_id;
    }

    /**
     * @param mixed $to_planet_id
     */
    public function setToPlanetId($to_planet_id): void
    {
        $this->to_planet_id = $to_planet_id;
    }

    /**
     * @return mixed
     */
    public function getPlanetSystemId()
    {
        return $this->planet_system_id;
    }

    /**
     * @param mixed $planet_system_id
     */
    public function setPlanetSystemId($planet_system_id): void
    {
        $this->planet_system_id = $planet_system_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("route");
        $this->belongsTo('pricelist_id', 'Pricelist', 'id', ['alias' => 'Pricelist']);
        $this->hasMany('id', 'RouteProvider', 'route_id', ['alias' => 'RouteProvider']);
    }
}
