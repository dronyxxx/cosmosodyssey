<?php

class CustomerReservation extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $pricelist_id;

    /**
     *
     * @var string
     */
    protected $firstname;

    /**
     *
     * @var string
     */
    protected $lastname;

    /**
     *
     * @var integer
     */
    protected $created_at;

    /**
     *
     * @var double
     */
    protected $total_price;

    /**
     *
     * @var integer
     */
    protected $total_travel_time;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field pricelist_id
     *
     * @param integer $pricelist_id
     * @return $this
     */
    public function setPricelistId($pricelist_id)
    {
        $this->pricelist_id = $pricelist_id;

        return $this;
    }

    /**
     * Method to set the value of field firstname
     *
     * @param string $firstname
     * @return $this
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Method to set the value of field lastname
     *
     * @param string $lastname
     * @return $this
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param integer $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Method to set the value of field total_price
     *
     * @param double $total_price
     * @return $this
     */
    public function setTotalPrice($total_price)
    {
        $this->total_price = $total_price;

        return $this;
    }

    /**
     * Method to set the value of field total_travel_time
     *
     * @param integer $total_travel_time
     * @return $this
     */
    public function setTotalTravelTime($total_travel_time)
    {
        $this->total_travel_time = $total_travel_time;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field pricelist_id
     *
     * @return integer
     */
    public function getPricelistId()
    {
        return $this->pricelist_id;
    }

    /**
     * Returns the value of field firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Returns the value of field lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Returns the value of field created_at
     *
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Returns the value of field total_price
     *
     * @return double
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * Returns the value of field total_travel_time
     *
     * @return integer
     */
    public function getTotalTravelTime()
    {
        return $this->total_travel_time;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("customer_reservation");
        $this->hasMany('id', 'ReservationRoute', 'customer_reservation_id', ['alias' => 'ReservationRoute']);
        $this->belongsTo('pricelist_id', 'Pricelist', 'id', ['alias' => 'Pricelist']);
    }

}
