<?php

class RoutePlanets extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $from_planet_id;

    /**
     *
     * @var integer
     */
    protected $to_planet_id;

    /**
     *
     * @var integer
     */
    protected $route_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field from_planet_id
     *
     * @param integer $from_planet_id
     * @return $this
     */
    public function setFromPlanetId($from_planet_id)
    {
        $this->from_planet_id = $from_planet_id;

        return $this;
    }

    /**
     * Method to set the value of field to_planet_id
     *
     * @param integer $to_planet_id
     * @return $this
     */
    public function setToPlanetId($to_planet_id)
    {
        $this->to_planet_id = $to_planet_id;

        return $this;
    }

    /**
     * Method to set the value of field route_id
     *
     * @param integer $route_id
     * @return $this
     */
    public function setRouteId($route_id)
    {
        $this->route_id = $route_id;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field from_planet_id
     *
     * @return integer
     */
    public function getFromPlanetId()
    {
        return $this->from_planet_id;
    }

    /**
     * Returns the value of field to_planet_id
     *
     * @return integer
     */
    public function getToPlanetId()
    {
        return $this->to_planet_id;
    }

    /**
     * Returns the value of field route_id
     *
     * @return integer
     */
    public function getRouteId()
    {
        return $this->route_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("route_planets");
        $this->belongsTo('from_planet_id', '\Planet', 'id', ['alias' => 'FromRoutePlanets']);
        $this->belongsTo('to_planet_id', '\Planet', 'id', ['alias' => 'ToRoutePlanets']);
        $this->belongsTo('route_id', '\Route', 'id', ['alias' => 'Route']);
    }

}
