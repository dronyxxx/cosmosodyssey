<?php

class ReservationRoute extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $customer_reservation_id;

    /**
     *
     * @var integer
     */
    protected $route_provider_id;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field customer_reservation_id
     *
     * @param integer $customer_reservation_id
     * @return $this
     */
    public function setCustomerReservationId($customer_reservation_id)
    {
        $this->customer_reservation_id = $customer_reservation_id;

        return $this;
    }


    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field customer_reservation_id
     *
     * @return integer
     */
    public function getCustomerReservationId()
    {
        return $this->customer_reservation_id;
    }

    /**
     * @return int
     */
    public function getRouteProviderId(): int
    {
        return $this->route_provider_id;
    }

    /**
     * @param int $route_provider_id
     */
    public function setRouteProviderId(int $route_provider_id): void
    {
        $this->route_provider_id = $route_provider_id;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("reservation_route");
        $this->belongsTo('customer_reservation_id', '\CustomerReservation', 'id', ['alias' => 'CustomerReservation']);
        $this->belongsTo('route_provider_id', '\RouteProvider', 'id', ['alias' => 'RouteProvider']);
    }
}
