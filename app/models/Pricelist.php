<?php

class Pricelist extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    protected $id;

    /**
     *
     * @var integer
     */
    protected $valid_until;

    /**
     *
     * @var string
     */
    protected $api_id;

    /**
     *
     * @var integer
     */
    protected $created_at;

    /**
     * Method to set the value of field id
     *
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Method to set the value of field valid_until
     *
     * @param integer $valid_until
     * @return $this
     */
    public function setValidUntil($valid_until)
    {
        $this->valid_until = $valid_until;

        return $this;
    }

    /**
     * Method to set the value of field api_id
     *
     * @param string $api_id
     * @return $this
     */
    public function setApiId($api_id)
    {
        $this->api_id = $api_id;

        return $this;
    }

    /**
     * Method to set the value of field created_at
     *
     * @param integer $created_at
     * @return $this
     */
    public function setCreatedAt($created_at)
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * Returns the value of field id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns the value of field valid_until
     *
     * @return integer
     */
    public function getValidUntil(): int
    {
        return $this->valid_until;
    }

    /**
     * Returns the value of field api_id
     *
     * @return string
     */
    public function getApiId()
    {
        return $this->api_id;
    }

    /**
     * Returns the value of field created_at
     *
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Initialize method for model.
     */
    public function initialize()
    {
        $this->setSource("pricelist");
        $this->hasMany('id', 'CustomerReservation', 'pricelist_id', ['alias' => 'CustomerReservation']);
        $this->hasMany('id', 'Route', 'pricelist_id', ['alias' => 'Route']);
    }
}
