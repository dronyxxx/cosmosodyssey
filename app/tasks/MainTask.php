<?php

namespace CosmosCli;

use Phalcon\Cli\Task;

class MainTask extends Task
{
    public function mainAction()
    {
        echo 'This is the default ' . __CLASS__ . ' and the default ' . __FUNCTION__ . ' ' . PHP_EOL;
    }
}