<div class="card">
    <div class="card-header" id="heading<?= $route_nr ?>">
        Planets to fly through:<br>
        <?php foreach ($route_planets as $route_planet) { ?>
            <?= $route_planet ?>&nbsp;
        <?php } ?>
        <button type="button" class="btn btn--button btn-primary btn-block reserve_button">
            reserve
        </button>
    </div>
    <div>
        <div class="card-body">
            <div>Total distance: <?= $total_distance ?></div>
            <div>Travel time: <?= $this->helper->seconds2human($travel_time) ?></div>
            <div>Total price: <?= $total_price ?></div>
            <div style="padding-top: 10px;">
                <b>Fight data:</b>
                <?php foreach ($route_array as $route_array_data) { ?>
                <div style="padding-bottom: 10px;" class="route_id" data-route-id="<?= $route_array_data->getId() ?>">
                    <div>
                        From planet: <?= $planet_system->getPlanetNameById($route_array_data->getRelated('Route')->getFromPlanetId()) ?>
                    </div>
                    <div>
                        To planet: <?= $planet_system->getPlanetNameById($route_array_data->getRelated('Route')->getToPlanetId()) ?>
                    </div>
                    <div>
                        Flight start time: <?= date('d.m.Y H:i:s', $route_array_data->getFlightStart()) ?>
                    </div>
                    <div>
                        Flight end time: <?= date('d.m.Y H:i:s', $route_array_data->getFlightEnd()) ?>
                    </div>
                    <div>
                        Flight price: <?= $route_array_data->getPrice() ?>
                    </div>
                    <div>
                       Provider: <?= $route_array_data->getRelated('Provider')->getName() ?>
                    </div>
                    <div>
                        Distance: <?= $route_array_data->getRelated('Route')->getDistance() ?>
                    </div>
                </div>

                <?php } ?>
            </div>
        </div>
    </div>
</div>