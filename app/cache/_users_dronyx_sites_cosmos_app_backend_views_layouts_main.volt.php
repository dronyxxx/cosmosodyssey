<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <meta charset="utf-8" />
    <?= $this->tag->getTitle() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="apple-mobile-web-app-title" content="Cosmos" />
    <meta name="application-name" content="Cosmos" />
    <meta name="theme-color" content="#ffffff" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-touch-fullscreen" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="default" />

    
        <link href="/public/assets/vendor/system.86377aa7.css" rel="stylesheet">
    
    
        <script src="/public/assets/vendor/system.64df9b92.js"></script>
    

    <?= $this->tag->stylesheetLink('public/assets/css/cosmos.min.css') ?>
</head>
<body>
    <?= $this->getContent() ?>
    <?= $this->tag->javascriptInclude('public/assets/js/cosmos.min.js') ?>
</body>
</html>