<div class="page-nav page-sidebar" data-init="sidebar">
    <div class="page-nav__panel page-nav__panel--primary scrollbar-sidebar">
        <header class="page-nav__header"><div class="page-nav__logo"><img src="/public/assets/img/logo_header_2021.png" width="70"></div></header>
        <nav role="navigation" aria-labelledby="PrimaryNavigationLabel">
            <h2 id="PrimaryNavigationLabel" class="helper--visually-hidden"><?= $default_translate->_('Main menu') ?></h2>
            <ol class="page-nav__list page-nav__list--primary overflow-auto">

                <?php if ($backend_auth->checkUserAccess('model_add') || $backend_auth->checkUserAccess('model_view') || $backend_auth->checkUserAccess('model_edit')) { ?>
                <li class="page-nav__item <?php if ($controller == 'index') { ?>current<?php } ?>">
                    <a class="page-nav__link " href="/index/birthdays">
                        <div class="page-nav__item-icon page-nav__item-notification">
                            <i class="fas fa-home fa-lg" <?php if ($controller == 'index') { ?>style="color:#14171c"<?php } ?>></i>
                        </div>
                        <span class="page-nav__text"><?= $default_translate->_('Dashboard') ?></span>
                    </a>
                </li>
                <?php } ?>

                <?php if ($backend_auth->checkUserAccess('model_add') || $backend_auth->checkUserAccess('booking_add') || $backend_auth->checkUserAccess('client_add') || $backend_auth->checkUserAccess('user_add')) { ?>
                <li class="page-nav__item">
                    <button class="page-nav__link" data-target="#modalAdd" id="add">
                        <div class="page-nav__item-icon">
                            <i class="fal fa-plus-square fa-lg"></i>
                        </div>
                        <span class="page-nav__text"><?= $default_translate->_('Add') ?></span>
                    </button>
                </li>
                <?php } ?>

                <?php if ($backend_auth->checkUserAccess('booking_add') || $backend_auth->checkUserAccess('booking_view') || $backend_auth->checkUserAccess('booking_edit')) { ?>
                <li class="page-nav__item <?php if ($controller == 'booking') { ?>current<?php } ?>">
                    <a class="page-nav__link " href="/booking">
                        <div class="page-nav__item-icon">
                            <i class="fal fa-calendar-alt fa-lg" <?php if ($controller == 'booking') { ?>style="color:#14171c"<?php } ?>></i>
                        </div>
                        <span class="page-nav__text"><?= $default_translate->_('Booking') ?></span>
                    </a>
                </li>
                <?php } ?>
                <?php if ($backend_auth->checkUserAccess('model_add') || $backend_auth->checkUserAccess('model_view') || $backend_auth->checkUserAccess('model_edit')) { ?>
                <li class="page-nav__item <?php if ($controller == 'model') { ?>current<?php } ?>">
                    <a class="page-nav__link " href="/model<?php if (isset($this->session->get('backend-identity')['permissions'][$this->session->get('agency')['id']]['talent_section_default_view']) && $this->session->get('backend-identity')['permissions'][$this->session->get('agency')['id']]['talent_section_default_view'] == 2) { ?>/indextwocol<?php } ?>">
                        <div class="page-nav__item-icon">
                            <i class="fal fa-user-circle fa-lg" <?php if ($controller == 'model') { ?>style="color:#14171c"<?php } ?>></i>
                        </div>
                        <span class="page-nav__text"><?= $default_translate->_('Talents') ?></span>
                    </a>
                </li>
                <?php } ?>
                <?php if ($backend_auth->checkUserAccess('can_send_package')) { ?>
                    <li class="page-nav__item <?php if ($controller == 'pack') { ?>current<?php } ?>">
                        <a class="page-nav__link " href="/pack">
                            <div class="page-nav__item-icon">
                                <i class="fal fa-briefcase fa-lg" <?php if ($controller == 'pack') { ?>style="color:#14171c"<?php } ?>></i>
                            </div>
                            <span class="page-nav__text"><?= $default_translate->_('Packages') ?></span>
                        </a>
                    </li>
                <?php } ?>
                <?php if ($backend_auth->checkUserAccess('client_add') || $backend_auth->checkUserAccess('client_view') || $backend_auth->checkUserAccess('client_edit')) { ?>
                <li class="page-nav__item <?php if ($controller == 'client') { ?>current<?php } ?>">
                    <a class="page-nav__link " href="/client">
                        <div class="page-nav__item-icon">
                            <i class="fal fa-street-view fa-lg" <?php if ($controller == 'client') { ?>style="color:#14171c"<?php } ?>></i>
                        </div>
                        <span class="page-nav__text"><?= $default_translate->_('Business partners') ?></span>
                    </a>
                </li>
                <?php } ?>

                    <li class="page-nav__item <?php if ($controller == 'marketing') { ?>current<?php } ?>">
                        <a class="page-nav__link " href="/marketing/posts">
                            <div class="page-nav__item-icon">
                                <i class="fal fa-at fa-lg" <?php if ($controller == 'marketing') { ?>style="color:#14171c"<?php } ?>></i>
                            </div>
                            <span class="page-nav__text"><?= $default_translate->_('Marketing') ?></span>
                        </a>
                    </li>


                <li class="page-nav__item <?php if ($controller == 'accounting') { ?>current<?php } ?>">
                    <a class="page-nav__link " href="/accounting/agentreport">
                        <div class="page-nav__item-icon">
                            <i class="fal fa-usd-circle fa-lg" <?php if ($controller == 'accounting') { ?>style="color:#14171c"<?php } ?>></i>
                        </div>
                        <span class="page-nav__text"><?= $default_translate->_('Accounting') ?></span>
                    </a>
                </li>

                <li class="page-nav__item <?php if ($controller == 'file') { ?>current<?php } ?>">
                    <a class="page-nav__link " href="/file/talent">
                        <div class="page-nav__item-icon">
                            <i class="fal fa-folder fa-lg" <?php if ($controller == 'file') { ?>style="color:#14171c"<?php } ?>></i>
                        </div>
                        <span class="page-nav__text"><?= $default_translate->_('Files') ?></span>
                    </a>
                </li>

                
                    
                        
                            
                        
                        
                    
                

                
                    
                        
                            
                        
                        
                    
                

                
                    
                        
                            
                        
                        
                    
                

                <li class="page-nav__item <?php if ($controller == 'settings') { ?>current<?php } ?>">
                    <a class="page-nav__link " href="/settings">
                        <div class="page-nav__item-icon">
                            <i class="fal fa-cog fa-lg" <?php if ($controller == 'settings') { ?>style="color:#14171c"<?php } ?>></i>
                        </div>
                        <span class="page-nav__text"><?= $default_translate->_('Settings') ?></span>
                    </a>
                </li>

                <li class="page-nav__item--spacer" role="separator"></li>
                <li class="page-nav__item page-nav__item--account">
                    <button class="page-nav__link btn-thumbnail" data-toggle="modal" data-target="#modalUserAccount">
                        <div class="page-nav__item-icon">
                            <span class="thumbnail-wrapper circular d40">
                               <span><?= $this->session->get('backend-identity')['initials'] ?></span>
                            </span>
                        </div>
                        <span class="page-nav__text"><?= $this->session->get('backend-identity')['name'] ?></span>
                    </button>
                </li>
                <li class="page-nav__item page-nav__item--group">
                    <button class="page-nav__link page-nav__link--network" <?php if ($agencies_count > 1) { ?> data-toggle="modal" <?php } ?> data-target="#modalAgencies">
                        <div class="page-nav__item-icon">
                            <span><?= $this->helper->getAgencyInitials() ?></span>
                        </div>
                        <span class="page-nav__text"><?= $this->session->get('agency')['name'] ?></span>
                    </button>
                </li>

            </ol>
        </nav>
    </div>
</div>